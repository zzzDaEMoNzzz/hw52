import React, { Component } from 'react';
import './Number.css';

class Number extends Component{
  render() {
    return (
      <div className="Number">
        <span>{this.props.value}</span>
      </div>
    );
  }
}

export default Number;