import React, { Component } from 'react';
import './App.css';
import Number from './components/number/Number';

class App extends Component {
  state = {
    numbers: [5, 6, 7, 8, 9]
  };

  getRandomNumbers = () => {
    const numbersArray = [...this.state.numbers];

    for (let i = 0; i < numbersArray.length; i++) {
      let rndNum = 0;
      while (!rndNum || numbersArray.includes(rndNum)) {
        rndNum = Math.floor(Math.random() * (36 - 5 + 1) + 5);
      }

      numbersArray[i] = rndNum;
    }

    numbersArray.sort((a, b) => a - b);

    this.setState({numbers: numbersArray});
  };

  createNumberElements = () => {
    const numberElementsArray = [];

    for (let i = 0; i < this.state.numbers.length; i++) {
      numberElementsArray.push(<Number value={this.state.numbers[i]} key={i} />);
    }

    return numberElementsArray;
  };

  render() {
    return (
      <div className="container">
        <button className="newNumbersBtn" onClick={this.getRandomNumbers}>New numbers</button>
        <div className="numbersBlock">
          {this.createNumberElements()}
        </div>
      </div>
    );
  }
}

export default App;
